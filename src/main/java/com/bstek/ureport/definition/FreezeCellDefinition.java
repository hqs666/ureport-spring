package com.bstek.ureport.definition;

public class FreezeCellDefinition {
	
	private Integer col;
	
	private Integer row;

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}
}
